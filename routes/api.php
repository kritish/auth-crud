<?php

use App\Http\Middleware;
use App\Models\Post;
use App\Models\User;
// use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', function (Request $request) {
    $email = $request->email;
    $password = $request->password;
    $hashedPassword = Hash::make($password, ['rounds' => 10]);
    $user = new User;
    $user->email = $email;
    $user->password = $hashedPassword;
    $user->save();
    // event(new Registered($user));
    return response()->json(["user" => $user]);
});

Route::post('/login', function (Request $request) {
    $email = $request->email;
    $password = $request->password;
    $user = User::where('email', $email)->first();
    if (!Hash::check($password, $user->password)) {
        return response()->json(["success" => false, "message" => "password does not match"], 401);
    }
    $token = $user->createToken('token-name');
    return response()->json(["token" => $token->plainTextToken]);
});

Route::middleware('auth:sanctum')->get('/protected-route', function () {
    return response()->json(["message" => "this is protected route, only authenticated user can access"]);
});

Route::middleware('auth:sanctum')->post('/create-post', function (Request $request) {
    $title = $request->title;
    $description = $request->description;
    $user = User::find($request->user())->first();
    $post = new Post(["title" => $title, "description" => $description]);
    $user->posts()->save($post);

    return response()->json(["post" => $post], 201);
});

Route::middleware('auth:sanctum')->get('/fetch-post/{id}', function (Request $request, $id) {
    $post = Post::find($id)->first();
    $user = User::find($request->user())->first();
    if ($user->id != $post->user_id) {
        return resonse()->json(["success" => false, "message" => "forbidden request"], 401);
    }
    return response()->json(["success" => true, "post" => $post]);
});

Route::middleware('auth:sanctum')->put('/update-post/{id}', function (Request $request, $id) {
    $post = Post::find($id)->first();
    if ($request->user()->id != $post->user_id) {
        return response()->json(["success" => false, "message" => "forbidden request"], 401);
    }
    $title = $request->title ? $request->title : $post->title;
    $description = $request->description ? $request->description : $post->description;
    $post->title = $title;
    $post->description = $description;
    $post->save();
    return response()->json(["success" => true, "post" => $post]);

});

Route::middleware('auth:sanctum')->delete('delete-post/{id}', function (Request $request, $id) {
    $post = Post::find($id)->first();
    if ($request->user()->id != $post->user_id) {
        return response()->json(["success" => false, 'message' => "forbidden request"], 401);
    }
    $post->delete();
    return response()->json(["success" => true, "message" => "post deleted"]);
});
